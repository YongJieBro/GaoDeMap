//
//  MAMapView+JMap.h
//  GaoDeDemo
//
//  Created by GAO on 16/8/11.
//  Copyright © 2016年 GAO. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
@interface MAMapView (JMap)
//清除大头针
- (void)removeAnnotationsFun;

//清除路线
- (void)removeOverlaysFun;

//清除大头针和路线
- (void)removeAnnotationsAndOverlays;

//显示指定区域
- (void)showAppointRegionCoordinate:(CLLocationCoordinate2D)coordinate andSpan:(MACoordinateSpan)span;

//画路线
- (void)drawPath:(NSArray *)pathsArr andPathNum:(NSInteger)pathNum;

//添加大头针
- (void)addPointAnnotation:(MATouchPoi *)poi;
@end
