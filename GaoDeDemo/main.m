//
//  main.m
//  GaoDeDemo
//
//  Created by GAO on 16/7/26.
//  Copyright © 2016年 GAO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
