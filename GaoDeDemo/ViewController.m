//
//  ViewController.m
//  GaoDeDemo
//
//  Created by GAO on 16/7/26.
//  Copyright © 2016年 GAO. All rights reserved.
//

#import "ViewController.h"
#import "SearchTableVC.h"
#import "AMapSearchObject+JMapSearch.h"
#import "MAMapView+JMap.h"
@interface ViewController ()<UISearchResultsUpdating, UISearchBarDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
{
    SearchTableVC * _tableVC;
    NSArray *pickData;
}

@property (nonatomic, strong) NSMutableArray  * annotationArray;
@property (nonatomic, strong) UIBarButtonItem * previousItem;           //上一个
@property (nonatomic, strong) UIBarButtonItem * nextItem;               //下一个
@property (nonatomic, strong) NSMutableArray  * pathsArr;               //路径数据
@property (nonatomic, assign) NSInteger         pathNum;
@property (nonatomic, assign) BOOL              isClickSearch;
@property (nonatomic, strong) UIPickerView    * pickerVeiw;

@end

@implementation ViewController
@synthesize search, annotationArray, _mapView, searchAPI, pathsArr, pathNum, isClickSearch;

- (void)initNavigationBar
{
    [self initLeftBarButtonItem];
    
    [self initRightBarButtonItem];
}
- (void)initSearchTable
{
    _tableVC = [[SearchTableVC alloc] initWithStyle:UITableViewStylePlain];
    _tableVC.mapView = _mapView;
}

- (void)initLeftBarButtonItem
{
    UIBarButtonItem * leftItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain
                                                                              target:self
                                                                              action:nil];
    self.navigationItem.leftBarButtonItem = leftItem;
}
- (void)initRightBarButtonItem
{
    UIBarButtonItem * RightItem = [[UIBarButtonItem alloc] initWithTitle:@"选择校区" style:UIBarButtonItemStylePlain
                                                                                   target:self
                                                                                   action:@selector(showOrHidePickView)];
    self.navigationItem.rightBarButtonItem = RightItem;
}
- (void)initSearch
{
    self.search = [[UISearchController alloc]initWithSearchResultsController:_tableVC];
    search.searchBar.frame = CGRectMake(self.search.searchBar.frame.origin.x, self.search.searchBar.frame.origin.y, self.search.searchBar.frame.size.width, 44.0);
    search.dimsBackgroundDuringPresentation = NO;          //是否添加半透明覆盖层
    search.hidesNavigationBarDuringPresentation = NO;       //是否隐藏导航栏
    search.obscuresBackgroundDuringPresentation = NO;       //搜索时，背景变模糊
//    search.hidesBottomBarWhenPushed = NO;
    search.searchResultsUpdater = self;
    search.searchBar.delegate = self;
    [search.searchBar setTintColor:[UIColor redColor]];  //搜索时光标颜色
    [search.searchBar setPlaceholder:@"输入地点或地址"];
    for (UIGestureRecognizer *tap in search.searchBar.gestureRecognizers)
    {
        [search.searchBar removeGestureRecognizer:tap];
    }
    
    for (UIView *view in  [[[search.searchBar subviews] objectAtIndex:0] subviews])
    {
        if ([view isKindOfClass:[NSClassFromString(@"UINavigationButton") class]])
        {
            UIButton * cancel =(UIButton *)view;
            [cancel setTitle:@"取消" forState:UIControlStateNormal];
        }
    }
    
    if (search.searchBar)
    {
        self.navigationItem.titleView = search.searchBar;
//        [self.navigationController.navigationBar addSubview:search.searchBar];
    }
    annotationArray = [NSMutableArray array];
}
//单击地图时显示出点击位置名称
- (void)mapView:(MAMapView *)mapView didTouchPois:(NSArray *)pois
{
    if (pois)
    {
        [_mapView removeAnnotationsAndOverlays];
        MATouchPoi * poi = [pois firstObject];
        [_mapView addPointAnnotation:poi];
    }
}
- (void)initMap
{
    _mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    _mapView.showsIndoorMap = YES;
    _mapView.delegate = self;
    _mapView.showsUserLocation = YES;                                           //使用定位服务
//   [_mapView setUserTrackingMode:MAUserTrackingModeFollowWithHeading];       //定位模式
    
//    _mapView.pausesLocationUpdatesAutomatically = NO;                     //设置即使应用被挂起，也持续定位
//    _mapView.allowsBackgroundLocationUpdates = YES;                       //允许持续定位
    
    //将指定学校设置为地图中心点120.08666039 30.30446546
//    [_mapView setCenterCoordinate:CLLocationCoordinate2DMake(30.30446546, 120.08666039)];
    self.definesPresentationContext = YES;
    [self.view addSubview:_mapView];
}
- (void)initToolBar
{
    self.navigationController.toolbarHidden = NO;
    self.navigationController.toolbar.barStyle = UIBarStyleDefault;
    self.navigationController.toolbar.translucent = YES;
    
    UIBarButtonItem *flexbleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                 target:self
                                                                                 action:nil];
    /* 上一个. */
    UIBarButtonItem *previousItem = [[UIBarButtonItem alloc] initWithTitle:@"上一个"
                                                                     style:UIBarButtonItemStyleDone
                                                                    target:self
                                                                    action:@selector(previousCourseAction)];
    self.previousItem = previousItem;
    
    /* 下一个. */
    UIBarButtonItem *nextItem = [[UIBarButtonItem alloc] initWithTitle:@"下一个"
                                                                 style:UIBarButtonItemStyleDone
                                                                target:self
                                                                action:@selector(nextCourseAction)];
    self.nextItem = nextItem;
    
    self.toolbarItems = [NSArray arrayWithObjects:flexbleItem, previousItem, flexbleItem, nextItem, flexbleItem, nil];
}
- (void)showOrHidePickView
{
    if (_pickerVeiw.hidden)
    {
        [_pickerVeiw setHidden:NO];
//        self.navigationController.toolbarHidden = YES;
    }
    else
    {
        [_pickerVeiw setHidden:YES];
//        self.navigationController.toolbarHidden = NO;
    }
}
- (UIPickerView *)pickerView
{
    self.pickerVeiw = [[UIPickerView alloc]initWithFrame:CGRectMake(self.view.bounds.size.width - 120, 64, 120, 120)];
//    紫金港，玉泉，西溪，华家池，之江
    pickData = [NSArray arrayWithObjects:@"紫金港校区", @"玉泉校区", @"西溪校区", @"华家池校区", @"之江校区", nil];
    _pickerVeiw.delegate = self;
    _pickerVeiw.dataSource = self;
    
    return _pickerVeiw;
}
#pragma mark  UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [pickData count];
}
#pragma mark UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        return pickData[row];
    }
    else
    {
        return pickData[row];
    }
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
//    if (component == 0) {
//        NSLog(@"%@",self.nameArray[row]);
//        [pickerView selectedRowInComponent:0];
//        //        //重新加载数据
//        //        [pickerView reloadAllComponents];
//        //        //重新加载指定列的数据
//        //        [pickerView reloadComponent:1];
//    }
//    else
//    {
//        NSLog(@"%@",self.iconArray[row]);
//    }
     NSString *str = pickData[row];
    if([str isEqualToString:@"紫金港校区"])
    {
        [self zhiJingGangCampus];
    }
    else if ([str isEqualToString:@"玉泉校区"])
    {
        [self yuQuanCampus];
    }
    else if ([str isEqualToString:@"西溪校区"])
    {
        [self xiXiCampus];
    }
    else if ([str isEqualToString:@"华家池校区"])
    {
        [self huaJiaChiCampus];
    }
    else if ([str isEqualToString:@"之江校区"])
    {
        [self zhiJiangCampus];
    }
}
#pragma mark 上一个条路径
- (void)previousCourseAction
{
    if (![pathsArr count])
    {
        return;
    }
    
    if (pathNum <= 0)
    {
        pathNum = 0;
        return;
    }
    else
    {
        pathNum--;
    }
    [_mapView removeOverlaysFun];
    [_mapView drawPath:pathsArr andPathNum:pathNum];
}
#pragma mark 下一个条路径
- (void)nextCourseAction
{
    if (![pathsArr count])
    {
        return;
    }
    
    if (pathNum >= ([pathsArr count] -1))
    {
        pathNum = ([pathsArr count] -1);
        return;
    }
    else
    {
        pathNum++;
    }
    [_mapView removeOverlaysFun];
    [_mapView drawPath:pathsArr andPathNum:pathNum];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //初始化路径数组
    pathsArr = [NSMutableArray array];
    //记录是否点击过搜索
    isClickSearch = NO;
    
    //初始化NavigationBar
    [self initNavigationBar];
    
    //初始化地图
    [self initMap];
    
    //先初始化表格因为初始化searchBar的时候需要用到
    [self initSearchTable];
    
    //初始化搜索框
    [self initSearch];
    
    //初始化toolbar
    [self initToolBar];
   
//    [self polygon];                 //构造多边形
    [self zhiJingGangCampus];

    
    //搜索服务
    [self searchService];
    
    [self.view addSubview:[self pickerView]];
    _pickerVeiw.hidden = YES;
}

- (void)zhiJingGangCampus
{
    //显示指定区域
    //需要一个中心点，和纵向跨度、横向跨度
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = 30.30446546;
    coordinate.longitude = 120.08666039;
    MACoordinateSpan span;
    span.latitudeDelta = 0.02154799;
    span.longitudeDelta = 0.001202924;
    [_mapView showAppointRegionCoordinate:coordinate andSpan:span];
}
- (void)yuQuanCampus
{
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = 30.26405277;
    coordinate.longitude = 120.12346029;
    MACoordinateSpan span;
    span.latitudeDelta = 0.01393703;
    span.longitudeDelta = 0.01281023;
    [_mapView showAppointRegionCoordinate:coordinate andSpan:span];
}
- (void)xiXiCampus
{
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = 30.27635808;
    coordinate.longitude = 120.14066935;
    MACoordinateSpan span;
    span.latitudeDelta = 0.00870958;
    span.longitudeDelta = 0.00955575;
    [_mapView showAppointRegionCoordinate:coordinate andSpan:span];
}
- (void)huaJiaChiCampus
{
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = 30.26979791;
    coordinate.longitude = 120.19596577;
    MACoordinateSpan span;
    span.latitudeDelta = 0.01367672;
    span.longitudeDelta = 0.00989199;
    [_mapView showAppointRegionCoordinate:coordinate andSpan:span];
}
- (void)zhiJiangCampus
{
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = 30.19258113;
    coordinate.longitude = 120.12537224;
    MACoordinateSpan span;
    span.latitudeDelta = 0.00763204;
    span.longitudeDelta = 0.0102675;
    [_mapView showAppointRegionCoordinate:coordinate andSpan:span];
}
- (void)searchService
{
    searchAPI = [[AMapSearchAPI alloc]init];

    searchAPI.delegate = self;
}

/* 退出键盘 */
- (void)exitKeyboard
{
    if ([search.searchBar isFirstResponder])
    {
        [search.searchBar resignFirstResponder];
    }
}

/*  返回浙大的轮廓点  多边形 */
- (NSArray *)returnZheDaPoint
{
    AMapGeoPoint *Point1 = [[AMapGeoPoint alloc]init];
    Point1.latitude = 30.31011561;
    Point1.longitude = 120.08065224;
    AMapGeoPoint *Point2 = [[AMapGeoPoint alloc]init];
    Point2.latitude = 30.29509105;
    Point2.longitude = 120.08273363;
    AMapGeoPoint *Point3 = [[AMapGeoPoint alloc]init];
    Point3.latitude = 30.30937463;
    Point3.longitude = 120.08902073;
    AMapGeoPoint *Point4 = [[AMapGeoPoint alloc]init];
    Point4.latitude = 30.29477609;
    Point4.longitude = 120.09251833;
    
    NSArray *piontArr = [NSArray arrayWithObjects:Point1, Point2, Point3, Point4, nil];
    
    return piontArr;
}

#pragma mark - UISearchResultsUpdating  搜索结果 delegate
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = [searchController.searchBar text];
    
//    [AMapSearchObject searchKeywords:searchString andCity:@"杭州" andSearchApi:_searchAPI]; //关键字搜索

    if(!searchString.length)
    {
        return;
    }
    _tableVC.tableView.hidden = YES;
    //搜索 关键字 和 搜索范围 搜索对象
    [AMapSearchObject searchPolygon:searchString andPiontsArray:[self returnZheDaPoint] andSearchApi:searchAPI];
}
#pragma mark UISearchBarDelegate 点击搜索 delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSString *keywords = searchBar.text;
    if(!keywords.length)
    {
        return;
    }
    [AMapSearchObject searchPolygon:keywords andPiontsArray:[self returnZheDaPoint] andSearchApi:searchAPI];
    isClickSearch = YES;
}
//点击搜索框关闭，清除大头针和路径
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [_mapView removeAnnotationsAndOverlays];
}

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar{
    
}
- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    
}

#pragma mark - AMapSearchDelegate  搜索结果delegate
/**
 *  当请求发生错误时，会调用代理的此方法.
 *
 *  @param request 发生错误的请求.
 *  @param error   返回的错误.
 */
- (void)AMapSearchRequest:(id)request didFailWithError:(NSError *)error
{
    
}

/**
 *  POI查询回调函数
 *
 *  @param request  发起的请求，具体字段参考 AMapPOISearchBaseRequest 及其子类。
 *  @param response 响应结果，具体字段参考 AMapPOISearchResponse 。
 */
- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    if(response.pois.count == 0)
    {
        return;
    }
    _tableVC.tableView.hidden = NO;
    //通过 AMapPOISearchResponse 对象处理搜索结果
    NSString *strCount = [NSString stringWithFormat:@"count: %zd",response.count];
    NSString *strSuggestion = [NSString stringWithFormat:@"Suggestion: %@", response.suggestion];
    NSString *strPoi = @"";
    
    for (AMapPOI *p in response.pois)
    {
        strPoi = [NSString stringWithFormat:@"%@\nPOI: %@", strPoi, p.description];
    }
    NSString *result = [NSString stringWithFormat:@"%@ \n %@ \n %@", strCount, strSuggestion, strPoi];
    NSLog(@"Place: %@", result);
//    if(isClickSearch)
//    {
//        isClickSearch = NO;
//        [_tableVC.tableView setHidden:YES];
//        for (AMapPOI *poi in response.pois)
//        {
//            [_mapView addPointAnnotation:poi];
//        }
//    }
//    else
//    {
        _tableVC.searchResult = response.pois;
        [_tableVC.tableView reloadData];
//    }
}

/**
 *  路径规划查询回调
 *
 *  @param request  发起的请求，具体字段参考 AMapRouteSearchBaseRequest 及其子类。
 *  @param response 响应结果，具体字段参考 AMapRouteSearchResponse 。
 */
- (void)onRouteSearchDone:(AMapRouteSearchBaseRequest *)request response:(AMapRouteSearchResponse *)response
{
    if(response.route == nil)
    {
        return;
    }
    if([response.route.paths count])
    {
        pathNum = 0;
        
        pathsArr = (NSMutableArray *)response.route.paths;
        
        [_mapView drawPath:pathsArr andPathNum:pathNum];
    }
}

#pragma mark - MAMapViewDelegate
- (MAOverlayView *)mapView:(MAMapView *)mapView viewForOverlay:(id <MAOverlay>)overlay
{
    if ([overlay isKindOfClass:[MAPolyline class]])
    {
        MAPolylineView *polylineView = [[MAPolylineView alloc] initWithPolyline:overlay];
        
        polylineView.lineWidth = 8.f;
        polylineView.strokeColor = [UIColor colorWithRed:0 green:0 blue:1 alpha:0.6];
        polylineView.lineJoinType = kCGLineJoinMiter;//连接类型
        polylineView.lineCapType = kCGLineCapSquare;//端点类型
        
        return polylineView;
    }
    return nil;
}

//生成大头针
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAPointAnnotation class]])
    {
        [self exitKeyboard]; //退出键盘
        
        [annotationArray addObject:annotation];
        
        static NSString * pointReuseIndentifier = @"pointReuseIndentifier";
        MAPinAnnotationView * annotationView = (MAPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndentifier];
        if (annotationView == nil)
        {
            annotationView = [[MAPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndentifier];
        }
        annotationView.canShowCallout = YES;       //设置气泡可以弹出，默认为NO
        annotationView.animatesDrop = YES;        //设置标注动画显示，默认为NO
        annotationView.pinColor = MAPinAnnotationColorPurple;
        
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 60, 50)];
        [btn setBackgroundColor:[UIColor greenColor]];
//        [btn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//        [btn setTintColor:[UIColor blueColor]];
//        btn.titleLabel.text = @"获取路线1";
//        [btn setBackgroundImage:[UIImage imageNamed:@"greenPin"] forState:UIControlStateNormal];
//        [btn setTitle:@"获取路线" forState:UIControlStateNormal];
        annotationView.rightCalloutAccessoryView = btn;
        return annotationView;
    }
    
    return nil;
}

/*!
 @brief 当选中一个annotation views时调用此接口
 @param mapView 地图View
 @param views 选中的annotation views
 */
-(void)mapView:(MAMapView *)mapView didSelectAnnotationView:(MAAnnotationView *)view
{
    CLLocationCoordinate2D coo = [view.annotation coordinate];
    NSLog(@"%f %f",coo.latitude,coo.longitude);
}

/*!
 @brief 标注view的accessory view(必须继承自UIControl)被点击时调用此接口
 @param mapView 地图View
 @param annotationView callout所属的标注view
 @param control 对应的control
 */
- (void)mapView:(MAMapView *)mapView annotationView:(MAAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    CLLocationCoordinate2D coo = [view.annotation coordinate];
    NSLog(@"%f %f",coo.latitude,coo.longitude);
    
    AMapDrivingRouteSearchRequest *request = [[AMapDrivingRouteSearchRequest alloc]init];
    
    //    request.origin = [AMapGeoPoint locationWithLatitude:mapView.centerCoordinate.latitude longitude:mapView.centerCoordinate.longitude];
    request.origin = [AMapGeoPoint locationWithLatitude:_mapView.userLocation.location.coordinate.latitude longitude:self._mapView.userLocation.location.coordinate.longitude];
    
    request.destination = [AMapGeoPoint locationWithLatitude:coo.latitude longitude:coo.longitude];
    
    request.strategy = 5;//距离优先
    request.requireExtension = YES;
    [searchAPI AMapDrivingRouteSearch:request];
}

- (void)polyline
{
    CLLocationCoordinate2D commonPolylineCoords[4];
    commonPolylineCoords[0].latitude = 39.832136;
    commonPolylineCoords[0].longitude = 116.34095;
    
    commonPolylineCoords[1].latitude = 39.832136;
    commonPolylineCoords[1].longitude = 116.42095;
    
    commonPolylineCoords[2].latitude = 39.902136;
    commonPolylineCoords[2].longitude = 116.42095;
    
    commonPolylineCoords[3].latitude = 39.902136;
    commonPolylineCoords[3].longitude = 116.44095;
}

@end
