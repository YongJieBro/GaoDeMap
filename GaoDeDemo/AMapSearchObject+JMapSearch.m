//
//  AMapSearchObject+JMapSearch.m
//  GaoDeDemo
//
//  Created by GAO on 16/8/4.
//  Copyright © 2016年 GAO. All rights reserved.
//

#import "AMapSearchObject+JMapSearch.h"

@implementation AMapSearchObject (JMapSearch)

/**
 *PIO搜索
 */

/**
 *周边搜索（圆形范围搜索）
 *查询关键字，多个关键字用“|”分割
 *中心点坐标
 *查询半径，范围：0-50000，单位：米 [default = 3000]
 */
+ (void)searchAround:(NSString *)keywords andLocation:(AMapGeoPoint *)location andRadius:(NSInteger)radius andSearchApi:(AMapSearchAPI *)searchAPI
{
    AMapPOIAroundSearchRequest *tipsRequest = [[AMapPOIAroundSearchRequest alloc] init];
    tipsRequest.keywords = keywords;
    tipsRequest.location = location;
    tipsRequest.radius = radius;

    [searchAPI AMapPOIAroundSearch:tipsRequest];
}


//周边搜索（多边形范围搜索）
+ (void)searchPolygon:(NSString *)keywords andPiontsArray:(NSArray *)array andSearchApi:(AMapSearchAPI *)searchAPI
{
    AMapPOIPolygonSearchRequest *polygon = [[AMapPOIPolygonSearchRequest alloc]init];
    
    AMapGeoPolygon *polygonS = [[AMapGeoPolygon alloc]init];
    polygonS.points = array;
    polygon.polygon = polygonS;
    polygon.keywords = keywords;
    [searchAPI AMapPOIPolygonSearch:polygon];
}

/**
 *输入提示搜索
 */
//输入提示搜索
+ (void)searchKeywords:(NSString *)keywords andCity:(NSString *)city andSearchApi:(AMapSearchAPI *)searchAPI
{
    AMapInputTipsSearchRequest *tipsRequest = [[AMapInputTipsSearchRequest alloc] init];
    tipsRequest.keywords = keywords;
    tipsRequest.city = city;
    //    发起输入提示搜索
    [searchAPI AMapInputTipsSearch: tipsRequest];
}
@end
