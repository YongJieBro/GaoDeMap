//
//  ViewController.h
//  GaoDeDemo
//
//  Created by GAO on 16/7/26.
//  Copyright © 2016年 GAO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
@interface ViewController : UIViewController<MAMapViewDelegate, AMapSearchDelegate>
//地图
@property(nonatomic, strong) MAMapView     * _mapView;
//搜索
@property(nonatomic, strong) AMapSearchAPI * searchAPI;
//搜索框
@property(nonatomic, strong) UISearchController * search;


@end

