//
//  MAMapView+JMap.m
//  GaoDeDemo
//
//  Created by GAO on 16/8/11.
//  Copyright © 2016年 GAO. All rights reserved.
//

#import "MAMapView+JMap.h"

//#import "AMapSearchObject.h"

@implementation MAMapView (JMap)
//清除大头针
- (void)removeAnnotationsFun
{
    if([self annotations])
    {
        [self removeAnnotations:self.annotations];
    }
}

//清除遮罩
- (void)removeOverlaysFun
{
    if([self overlays])
    {
        [self removeOverlays:self.overlays];
    }
}

//清除大头针和路线
- (void)removeAnnotationsAndOverlays
{
    [self removeAnnotationsFun];
    [self removeOverlaysFun];
}

//显示指定区域
- (void)showAppointRegionCoordinate:(CLLocationCoordinate2D)coordinate andSpan:(MACoordinateSpan)span
{
    MACoordinateRegion region;
    region.center = coordinate;
    region.span = span;
    [self setRegion:region animated:YES];
}

- (void)drawPath:(NSArray *)pathsArr andPathNum:(NSInteger)pathNum
{
    //路线信息
    NSArray *steps = [[pathsArr objectAtIndex:pathNum] steps];
    
    for (AMapStep *step in steps)
    {
        //分成经纬度数组
        NSArray *pointArray = [step.polyline componentsSeparatedByString:@";"];
        //每组有多个经纬度点
        CLLocationCoordinate2D commonPolylineCoords[[pointArray count]];
        
        int index = 0;
        for (NSString *point in pointArray)
        {
            NSRange range = [point rangeOfString:@","];
            NSString *longitudeStr = [point substringToIndex:range.location];
            NSString *latitudeStr = [point substringFromIndex:range.location+1];
            commonPolylineCoords[index].longitude = longitudeStr.floatValue;
            commonPolylineCoords[index].latitude = latitudeStr.floatValue;
            index++;
        }
        MAPolyline *commonPolyline = [MAPolyline polylineWithCoordinates:commonPolylineCoords count:[pointArray count]];
        //在地图上添加折线对象
        [self addOverlay: commonPolyline];
    }
}

- (void)addPointAnnotation:(MATouchPoi *)poi;
{
    MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
    pointAnnotation.coordinate = CLLocationCoordinate2DMake(poi.coordinate.latitude, poi.coordinate.longitude);
    pointAnnotation.title = poi.name;
//    pointAnnotation.subtitle = poi.address;
    [self addAnnotation:pointAnnotation];
}
@end
