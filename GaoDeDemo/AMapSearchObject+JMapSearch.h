//
//  AMapSearchObject+JMapSearch.h
//  GaoDeDemo
//
//  Created by GAO on 16/8/4.
//  Copyright © 2016年 GAO. All rights reserved.
//

#import <AMapSearchKit/AMapSearchKit.h>

@interface AMapSearchObject (JMapSearch)

/**
 *POI搜索
 */
//周边搜索（多边形范围搜索）
+ (void)searchPolygon:(NSString *)keywords andPiontsArray:(NSArray *)array andSearchApi:(AMapSearchAPI *)searchAPI;
/**
 *输入提示搜索
 */
//关键字搜索
+ (void)searchKeywords:(NSString *)keywords andCity:(NSString *)city andSearchApi:(AMapSearchAPI *)Api;
@end
